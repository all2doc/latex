---
title: "使用VSCode编辑Latex"
slug: "Latex"
date: 2022-05-03T10:55:02+08:00
draft: false
---

首先需要一个基础的LaTex环境

关于LaTex：

LaTeX（/ˈlɑːtɛx/，常被读作/ˈlɑːtɛk/或/ˈleɪtɛk/，写作“LATEX”），是一种基于TEX的排版系统，由美国计算机科学家莱斯利·兰伯特在20世纪80年代初期开发，利用这种格式系统的处理，即使用户没有排版和程序设计的知识也可以充分发挥由TEX所提供的强大功能，不必一一亲自去设计或校对，能在几天，甚至几小时内生成很多具有书籍质量的印刷品。对于生成复杂表格和数学公式，这一点表现得尤为突出。因此它非常适用于生成高印刷质量的科技和数学、物理文档。这个系统同样适用于生成从简单的信件到完整书籍的所有其他种类的文档。

[LaTeX - 维基百科，自由的百科全书](https://zh.wikipedia.org/zh-cn/LaTeX)

安装LaTex：

TeX Live 是 TUG (TeX User Group) 维护和发布的 TeX 系统，可说是「官方」的 TeX 系统。我们推荐任何阶段的 TeX 用户，都尽可能使用 TeX Live，以保持在跨操作系统平台、跨用户的一致性。TeX Live 的官方站点是 [TeX Live - TeX Users Group](https://tug.org/texlive/)。

Windows下，您可以直接使用[Windows - TeX Live - TeX Users Group](https://tug.org/texlive/windows.html)官方网站的安装工具，但TeXLive安装包大小共有4G，下载可能较慢。如果出现此情况，可以转到[Index of /CTAN/systems/texlive/Images/ | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/texlive/Images/)下载。

![](2022-05-03-11-07-00.png)

下载完成后，点击install-tl-windows.bat文件进行安装即可，安装过程大约20分钟。

检查安装是否正常： 按win + R 打开运行，输入cmd，打开命令行窗口；然后输入命令xelatex -v ，如下图

## 图片处理

对图片的处理需要引入

[LaTeX入门(八)——图片](https://zhuanlan.zhihu.com/p/56435541)

插入图片
要在文档中插入图片，需要graphicx宏包。此外，为了让图片出现在正确的位置，需要float宏包。因此，在导言区中加上

\usepackage{graphicx}
\usepackage{float}
接着，如果我们要插入的图片的名字是pic.png, 那么在我们正文中要插入图片的位置加上如下代码：

\begin{figure}[H]
\centering
\includegraphics{pic.png}
\caption{Title of picture}
\end{figure}
这里注意一点，如果要将图片插入到文档中，建议将图片拷贝到.tex文件的同一级文件夹中，否则需要在\includegraphics{}的参数中填写绝对路径。

而\caption{}指令的作用，是在其对应的位置（位于\includegraphics{}前就是图片上方，后就是图片下方）产生“图x：Title of picture”的语句。其中x是图片的编号。关于图片的编号，后面的文章会统一说。

可以给\includegraphics{}加上option列表以调整图片的大小。比如说，我想插入的图片是原图片等比例缩小为原来的0.8，则可以写成

\includegraphics[scale=0.8]{pic.png} 
如果我是想专门调整长宽，则可以把scale=0.8换成height=xxx, width=xxx这样来实现。

此外，关于\caption{}，我们可以对其字体、文字进行相关设置。这里需要我们使用caption宏包。我们首先在导言区中写上

\usepackage{caption}
然后在导言区中使用\captionsetup{}进行相关设置。

由于我们这里需要设置的是figure浮动体的caption，因此，我们需要特别注明captionsetup[figure]{}. 如果我们需要改变标题中的“图x”，让它变成“Picture x”或者其他文字，在大括号中可以写name=Picture来实现。如果我们要改变这个caption整体的字号、字体，比如说全都变成Large字号，意大利斜体，则可以在大括号中写font={Large, it}. 值得注意的是这里的字号只能设置成LaTeX内置的那几个字号（可以参见之前的文章），而不能自己设置字号。其他参数均可以看caption宏包的说明。

并排图片
将两个图片并排在一起，需要一定的技巧。假设我们有两张图片pic1.png和pic2.png, 我们要将它们并排，则可以在正文中使用如下语句：

\begin{figure}[H]
\centering
\begin{minipage}{0.48\textwidth}
\centering
\includegraphics{pic1.png}
\caption{Title of pic1}
\end{minipage}
\begin{minipage}{0.48\textwidth}
\centering
\includegraphics{pin2.png}
\caption{Title of pic2}
\end{minipage}
\end{figure}
如果我们要并排三张图，则将0.48\textwidth变成0.32\textwidth，以此类推。

过宽的图片
有时候我们会遇到过宽的图片，由于我们之前说的页面设置中存在左边距，所以会出现以下这种尴尬情况：


这时，我们需要使用一个叫adjustbox的宏包。在导言区加上一句

\usepackage[export]{adjustbox} 
然后在正文中使用

\begin{figure}[H]
\centering
\includegraphics[center]{pic.png}
\end{figure}
即可。也就是说，在\includegraphics[]{}的中括号中，加上一句center就能实现图片居中了。如图：

## 资源

stackexchange有tex板块，内容丰富。

[TeX - LaTeX Stack Exchange](https://tex.stackexchange.com/)

## Overleaf

Overleaf是一个云端协作式LaTeX编辑器，可用于编写和发布论文。这一编辑器与很多科学杂志出版商有合作关系，不但提供官方期刊的LaTeX模板，还能直接将文件提交至这些出版社。

[Overleaf - 维基百科，自由的百科全书](https://zh.wikipedia.org/zh-cn/Overleaf)


![](2022-05-03-17-06-12.png)

使用 Overleaf 的一大好处是可以方便地套用模板。转到[Templates - Journals, CVs, Presentations, Reports and More](https://www.overleaf.com/latex/templates)

![](2022-05-03-17-07-51.png)

## LaTeX 语法简介

此部分可以参考 overleaf 提供的30分钟入门指南。

英文原版：

中文翻译版：[Overleaf 指南：30 分钟 LaTeX 入门 &#8211; 章天杰. 16A086.](https://imztj.cn/?p=3073)



### 编译链

在LaTex中，文章内容、参考文献文件、宏包文件、格式文件等是互相分开的。编译过程中，需要将这些文件一个个拼接起来。这个工作不是一步到位的。于是就有了编译链这个说法：将前一步编译的结果输送到下一步继续编译。


#### Latex中的各种文件
tex：tex文件是最常见的latex文件，也是平时编写文章的文件
cls：cls文件是latex的格式文件，规定了tex源文件的排版格局，称为类文件（class），一般使用\documentclass{}导入
sty：sty文件是宏包文件（package），一般使用\usepackage{}导入
bst：bst文件是参考文件的格式文件，一般使用\bibliographystyle{}导入
bib：bib文件是参考文献的库，一般使用\bibliography{}导入

bib文件一般如下：

```
@article{XXX,
  title={ABC},
  author={A, B},
  journal={XX},
  year={20XX}
}
@inproceedings{YYY,
  title={ABC},
  author={A, B, C},
  booktitle={YY},
  pages={a--b},
  year={20YY}
}
```

面举例一个包含引入上述类型文件的tex文件模版

假设在当前目录下有下列文件：main.tex、A.cls、B.sty、C.bst、D.bib

%main.tex文件
\documentclass{A}     % 或者不使用自定义的排版文件时，使用最普通的\documentclass{article}
\usepackage{B}        % 以及导入一些其他常用的宏文件，如amsmath、amssymb、amsthm等数学相关的宏文件
\begin{document}
XXX
XXX
XXX
% 正文结束
\bibliography{D}      % 导入正文中引入文献的数据
\bibliographystyle{C} % 导入参考文献的格式文件C.bst
\end{document}


#### 编译过程

附带参考文献的整个编译需要四步。

(xe/pdf)latex main.tex   # 表示使用 latex, pdflatex 或 xelatex 编译，下同
bibtex main.aux
(xe/pdf)latex main.tex
(xe/pdf)latex main.tex


第一步latex，生成main.aux、main.log和main.pdf文件。其中aux是引用标记记录文件，用于再次编译时生成参考文献和超链接。此时的pdf文件中没有包含参考文件，在正文中的引用后为[?]。

第二步bibtex，生成main.bbl和main.blg文件。blg为bibtex处理过程记录文件。

第三步latex，更新了main.aux、main.log和main.pdf文件。此时的pdf文件的末尾已经有了参考文献列表，但是在正文中的引用后仍然为[?]。

第四步latex，更新了main.aux、main.log和main.pdf文件。并生成最终的pdf文件，此时正文中的引用后已经标记好了引用文献的序号[1]、[2]等。
————————————————

###

PDFLaTeX 编译模式与 XeLaTeX 区别如下： 1. PDFLaTeX 使用的是TeX的标准字体，所以生成PDF时，会将所有的非 TeX 标准字体进行替换，其生成的 PDF 文件默认嵌入所有字体；而使用 XeLaTeX 编译，如果说论文中有很多图片或者其他元素没有嵌入字体的话，生成的 PDF 文件也会有些字体没有嵌入。 2. XeLaTeX 对应的 XeTeX 对字体的支持更好，允许用户使用操作系统字体来代替 TeX 的标准字体，而且对非拉丁字体的支持更好。 3. PDFLaTeX 进行编译的速度比 XeLaTeX 速度快。

作者：Ali-loner
链接：https://zhuanlan.zhihu.com/p/166523064
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

## 附录

### Reference



### 版权声明

本文原载于https://latex.all2doc.com/
