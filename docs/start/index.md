---
title: "LaTeX 快速开始"
slug: "Latex"
date: 2022-05-03T10:55:02+08:00
draft: false
---

## 快速开始

LaTeX 是科研论文写作的标配。

通过 Overleaf，可以快速、在线体验 LaTeX。

转到：[Register](https://www.overleaf.com/register) 注册。

如下图，从例子创建：

![](2022-05-04-13-51-17.png)

转到编辑区域：

![](2022-05-04-13-53-35.png)

您可以参照所给例子进行更改、渲染、查看结果，所给示例已经非常详细，您只需更改其中文本内容，做替换即可。完成修改后，**左上角 点击 Menu 可以下载 PDF 文档**：

![](2022-05-04-13-55-18.png)

## 下一步

- [LaTeX 详细介绍](https://latex.all2doc.com/intro/)
- [三十分钟入门 LaTeX 语法](https://latex.all2doc.com/30min/)
- [本地安装LaTex（VSCode）](https://latex.all2doc.com/vscode/)
- [配置参考文献](https://latex.all2doc.com/ref/)
<!-- - 配置公式：
- 配置图片：
- 学习资源：
- 工具与插件： -->

## 附录

### Reference

1. [客_texlive安装需要多久](https://blog.csdn.net/qq_34769162/article/details/119752831)
2. [Visual Studio Code (vscode)配置LaTeX](https://zhuanlan.zhihu.com/p/166523064)
3. [LaTeX公式手册(全网最全) - 樱花赞 - 博客园](https://www.cnblogs.com/1024th/p/11623258.html)
4. [Latex中的各种文件及编译流程（附windows环境的完整编译脚本）_huitailangyz的博客-CSDN博客_latex编译](https://blog.csdn.net/huitailangyz/article/details/99685683)
5. [构建一个舒适的 LaTeX 工作流](https://blog.triplez.cn/posts/build-a-great-latex-workflow/)


### 版权声明

本文原载于https://latex.all2doc.com/
