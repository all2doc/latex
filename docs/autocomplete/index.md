---
title: "VSCode配置公式自动补全"
slug: "Autocomplete"
date: 2022-05-04T12:54:09+08:00
draft: false
---

[配置 LaTeX 公式自动补全 - 樱花赞 - 博客园](https://www.cnblogs.com/1024th/p/15332686.html)

使用 首先安装 HyperSnips - Visual Studio Marketplace 插件。

[GitHub - OrangeX4/OrangeX4-HyperSnips: OrangeX4&#39;s HyperSnips](https://github.com/OrangeX4/OrangeX4-HyperSnips)

[latex---vscode编辑器配置及快捷键（snnipets）设置](https://zhuanlan.zhihu.com/p/350249305)

[math-snippets/snippets.json at master · thomanq/math-snippets](https://github.com/thomanq/math-snippets/blob/master/snippets/snippets.json)

这是非常爽的技能，如视频所示（视频不知道为啥被挂了...）

在乙醇兄的帮助下顺利地完成了这个调试，还发现了一个插件也很好用叫Math Snippets，足够实现日常需要了，但是有些键位还是自己习惯地比较好，所以我们可以复制插件的代码做自己的更改

首先我们按ctrl+shift+p, 输入snippet点击配置用户代码片段

## 附录

### Reference

### 版权声明

本文原载于https://latex.all2doc.com/
