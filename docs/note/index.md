---
title: "Royse 的 LaTeX 笔记"
slug: "Note"
date: 2022-05-26T22:24:15+08:00
draft: true
---


## 各种语法

### 花体 

标准的Latex“书法”字体
无需额外的宏包：

写法
$ \mathcal{A}$；
————————————————
版权声明：本文为CSDN博主「Mr. coder」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/LiBo011010921/article/details/105737397

### 微分号

\mathrm{d}

[关于LaTeX中的微分算符d](https://zhuanlan.zhihu.com/p/413407561)

### 矩阵

[如何用latex编写矩阵（包括各类复杂、大型矩阵）？](https://zhuanlan.zhihu.com/p/266267223)


## 方程组 

\begin{equation}
\begin{cases}
a+b=2\\
a-b=4\\
\end{cases}
\end{equation}

[latex����ô���뷽���飨�������ţ�����룩_�ٶ�֪��](https://zhidao.baidu.com/question/236291243.html#:~:text=Latex%E8%BE%93%E5%85%A5%E6%8B%A5%E6%9C%89%E5%B7%A6%E6%8B%AC%E5%8F%B7%E4%B8%94%E5%B7%A6%E5%AF%B9%E9%BD%90%E7%9A%84%E6%96%B9%E7%A8%8B%E7%BB%84%E9%9C%80%E8%A6%81%E4%BD%BF%E7%94%A8left%2Fright%2Cbegin%2Fend%E8%BF%99%E4%B8%A4%E5%AF%B9%E7%BB%84%E5%90%88%EF%BC%8C%E5%85%B7%E4%BD%93%E6%96%B9%E6%B3%95%E4%B8%BA%EF%BC%9A%201%E3%80%81%E9%A6%96%E5%85%88%E9%9C%80%E8%A6%81%E5%9C%A8%E4%BB%A3%E7%A0%81%E7%9A%84%E6%9C%80%E5%A4%96%E5%9B%B4%E7%94%A8begin,%7Bequation%7D-end%20%7Bequation%7D%E6%9D%A5%E5%B0%86%E6%95%B4%E4%B8%AA%E4%BB%A3%E7%A0%81%E5%9D%97%E5%9B%B4%E8%B5%B7%E6%9D%A5%E3%80%82%202%E3%80%81%E6%8E%A5%E7%9D%80%E5%86%8D%E4%BD%BF%E7%94%A8left-right%E6%9D%A5%E4%B8%BA%E6%96%B9%E7%A8%8B%E7%BB%84%E7%9A%84%E4%BD%8D%E7%BD%AE%E8%BF%9B%E8%A1%8C%E8%AE%BE%E7%BD%AE%E3%80%82)

不编号

\nonumber的位置是在公式中，第一行和第二行都可以
[Latex公式换行且不加编号_云灬沙的博客-CSDN博客_latex公式不编号](https://blog.csdn.net/qq_38123961/article/details/111315060)


## 平均值

\overline 能覆盖所有括号中的文本，\bar 长度只能覆盖一个字母。

以上

[怎么在Latex里打x的平均值(即x上有一横杠）？ - 知乎](https://www.zhihu.com/question/19784137)



## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
