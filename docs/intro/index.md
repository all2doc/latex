---
title: "初识Latex"
slug: "Intro"
date: 2022-05-04T11:26:35+08:00
draft: false
---

## 什么是Latex

>LaTeX（/ˈlɑːtɛx/，常被读作/ˈlɑːtɛk/或/ˈleɪtɛk/，写作“LATEX”），是一种基于TEX的**排版系统**，由美国计算机科学家莱斯利·兰伯特在20世纪80年代初期开发，利用这种格式系统的处理，即使用户没有排版和程序设计的知识也可以充分发挥由TEX所提供的强大功能，不必一一亲自去设计或校对，能在几天，甚至几小时内生成很多具有书籍质量的印刷品。对于生成复杂表格和数学公式，这一点表现得尤为突出。因此它非常适用于生成高印刷质量的科技和数学、物理文档。这个系统同样适用于生成从简单的信件到完整书籍的所有其他种类的文档。——[LaTeX - 维基百科，自由的百科全书](https://zh.wikipedia.org/zh-cn/LaTeX)

Latex:

- 类似于 Word，是一款用于书写论文、文章的工具。然而，却使用了完全不同于所见即所得的，另一种思路。
- 类似于脚本语言，需要经过多次编译解释才能生成PDF等格式的文档。不过只能生成文档。
- 类似于 Markdown，本身是纯文本格式，通过各种语法生成漂亮的格式。但比 Markdown更加强大。

对于大多数人而言，曾经用过的文档编辑工具一般也就 记事本 和 Word 两种。Word 最大的特点，就是文档格式和软件本体紧密联系，格式由软件处理、隐藏在设置里面了。所有在 docx 中设置的格式，都需要再利用 Word 打开，点击许多选项卡，一点点进行调节。如果你要切换整体文档的风格，就不得不对每一个细节进行调节。

而 LaTeX 将格式设置和文本写在了一起，你可以直接用记事本打开 LaTeX 源文件。也可以很方便地调节各种展示格式和模板。另外， LaTeX 对数学公式和表格、图片、参考文献的支持也非常好。LaTeX 是科研论文的标准格式。

需要注意的是，LaTeX 和基于 TeX 的公式输入是两个概念。在 matlab 中我们实际上使用的是 Tex 公式。

## 使用LaTex

### 在线——Overleaf

>Overleaf是一个云端协作式LaTeX编辑器，可用于编写和发布论文。这一编辑器与很多科学杂志出版商有合作关系，不但提供官方期刊的LaTeX模板，还能直接将文件提交至这些出版社。——[Overleaf - 维基百科，自由的百科全书](https://zh.wikipedia.org/zh-cn/Overleaf)

[Overleaf, Online LaTeX Editor](https://www.overleaf.com)

![](2022-05-03-17-06-12.png)

Overleaf 已经预装好了 LaTeX 在线环境，方便您尝试 LaTeX、学习 LaTeX 语法而无需进行繁杂的安装配置过程。

使用 Overleaf 的一大好处是可以方便地套用模板。转到[Templates - Journals, CVs, Presentations, Reports and More](https://www.overleaf.com/latex/templates)

![](2022-05-03-17-07-51.png)

### 本地——Tex Live + VSCode

本地推荐使用 Tex Live 配合 VSCode 使用。详细过程请参考 [Vscode - 基础知识](https://latex.all2doc.com/vscode/)



## 关于 Tex 家族

到目前为止，我们已经听说了 Tex、LaTeX、XeLaTeX、PDFLaTeX等等概念。实际上，Tex 是一个大家族。其祖宗就是 Tex，一个排版规范和工具，在最基础的 Tex 上，LaTeX 进行了封装，使 TeX 更加易于使用。而 **LaTeX 则是对其的进一步扩充。

关于 LaTeX、MiKTeX 等等，请参考以下网页：

1. [Levels of TeX - TeX Users Group](https://tug.org/levels.html) 关于 Tex 家族
2. [TeX和LaTeX关系：原生TeX实现LaTeX命令](https://zhuanlan.zhihu.com/p/202799658) LaTeX 的原理
3. [What is the difference between TeX and LaTeX?](https://tex.stackexchange.com/questions/49/what-is-the-difference-between-tex-and-latex)

## LaTeX 语法简介

此部分可以参考 overleaf 提供的30分钟入门指南。

英文原版：[Learn LaTeX in 30 minutes](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes)

中文翻译版：[Overleaf 指南：30 分钟 LaTeX 入门 &#8211; 章天杰. 16A086.](https://imztj.cn/?p=3073)


## 附录

### Reference

1. [客_texlive安装需要多久](https://blog.csdn.net/qq_34769162/article/details/119752831)
2. [Visual Studio Code (vscode)配置LaTeX](https://zhuanlan.zhihu.com/p/166523064)
3. [LaTeX公式手册(全网最全) - 樱花赞 - 博客园](https://www.cnblogs.com/1024th/p/11623258.html)
4. [Latex中的各种文件及编译流程（附windows环境的完整编译脚本）_huitailangyz的博客-CSDN博客_latex编译](https://blog.csdn.net/huitailangyz/article/details/99685683)


### 版权声明

本文原载于https://latex.all2doc.com/


## 附录

### Reference

### 版权声明

本文原载于https://latex.all2doc.com/
