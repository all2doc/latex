---
title: "LaTeX 编译器与编译链"
slug: "Recipes"
date: 2022-05-04T15:11:04+08:00
draft: false
---

此部分将介绍 LaTeX 中与编译、编译链 相关的内容。

**如果您按照本教程其他部分进行配置，则已经默认设置好了编译工具，即可以跳过此部分。**

## 编译器



## 编译链

在LaTex中，文章内容、参考文献文件、宏包文件、格式文件等是互相分开的。编译过程中，需要将这些文件一个个拼接起来。这个工作不是一步到位的。于是就有了编译链这个说法：将前一步编译的结果输送到下一步继续编译。

#### Latex中的各种文件
tex：tex文件是最常见的latex文件，也是平时编写文章的文件
cls：cls文件是latex的格式文件，规定了tex源文件的排版格局，称为类文件（class），一般使用\documentclass{}导入
sty：sty文件是宏包文件（package），一般使用\usepackage{}导入
bst：bst文件是参考文件的格式文件，一般使用\bibliographystyle{}导入
bib：bib文件是参考文献的库，一般使用\bibliography{}导入

bib文件一般如下：

```
@article{XXX,
  title={ABC},
  author={A, B},
  journal={XX},
  year={20XX}
}
@inproceedings{YYY,
  title={ABC},
  author={A, B, C},
  booktitle={YY},
  pages={a--b},
  year={20YY}
}
```

面举例一个包含引入上述类型文件的tex文件模版

假设在当前目录下有下列文件：main.tex、A.cls、B.sty、C.bst、D.bib

%main.tex文件
\documentclass{A}     % 或者不使用自定义的排版文件时，使用最普通的\documentclass{article}
\usepackage{B}        % 以及导入一些其他常用的宏文件，如amsmath、amssymb、amsthm等数学相关的宏文件
\begin{document}
XXX
XXX
XXX
% 正文结束
\bibliography{D}      % 导入正文中引入文献的数据
\bibliographystyle{C} % 导入参考文献的格式文件C.bst
\end{document}


#### 编译过程

附带参考文献的整个编译需要四步。

(xe/pdf)latex main.tex   # 表示使用 latex, pdflatex 或 xelatex 编译，下同
bibtex main.aux
(xe/pdf)latex main.tex
(xe/pdf)latex main.tex


第一步latex，生成main.aux、main.log和main.pdf文件。其中aux是引用标记记录文件，用于再次编译时生成参考文献和超链接。此时的pdf文件中没有包含参考文件，在正文中的引用后为[?]。

第二步bibtex，生成main.bbl和main.blg文件。blg为bibtex处理过程记录文件。

第三步latex，更新了main.aux、main.log和main.pdf文件。此时的pdf文件的末尾已经有了参考文献列表，但是在正文中的引用后仍然为[?]。

第四步latex，更新了main.aux、main.log和main.pdf文件。并生成最终的pdf文件，此时正文中的引用后已经标记好了引用文献的序号[1]、[2]等。
————————————————

### PDFLaTeX 和 XeLaTeX

PDFLaTeX 编译模式与 XeLaTeX 区别如下： 1. PDFLaTeX 使用的是TeX的标准字体，所以生成PDF时，会将所有的非 TeX 标准字体进行替换，其生成的 PDF 文件默认嵌入所有字体；而使用 XeLaTeX 编译，如果说论文中有很多图片或者其他元素没有嵌入字体的话，生成的 PDF 文件也会有些字体没有嵌入。 2. XeLaTeX 对应的 XeTeX 对字体的支持更好，允许用户使用操作系统字体来代替 TeX 的标准字体，而且对非拉丁字体的支持更好。 3. PDFLaTeX 进行编译的速度比 XeLaTeX 速度快。

作者：Ali-loner
链接：https://zhuanlan.zhihu.com/p/166523064
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

## 附录

### Reference

1. https://blog.csdn.net/Cai_deLong/article/details/113970205

### 版权声明

本文原载于https://latex.all2doc.com/
