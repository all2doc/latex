---
title: "LaTeX 处理参考文献"
slug: "Ref"
date: 2022-05-04T12:43:54+08:00
draft: false
---

LaTeX插入参考文献，可以使用BibTex，也可以不使用BibTex。

## 不使用BibTeX

先在文章文章末尾写好需要插入的参考文献，逐一写出，例如：

```latex
begin{thebibliography}{99}  

bibitem{ref1}郭莉莉,白国君,尹泽成,魏惠芳. “互联网+”背景下沈阳智慧交通系统发展对策建议[A]. 中共沈阳市委、沈阳市人民政府.第十七届沈阳科学学术年会论文集[C].中共沈阳市委、沈阳市人民政府:沈阳市科学技术协会,2020:4.
bibitem{ref2}陈香敏,魏伟,吴莹. “文化+人工智能”视阈下文化创意产业融合发展实践及路径研究[A]. 中共沈阳市委、沈阳市人民政府.第十七届沈阳科学学术年会论文集[C].中共沈阳市委、沈阳市人民政府:沈阳市科学技术协会,2020:4.
bibitem{ref3}田晓曦,刘振鹏,彭宝权. 地方高校开展教育人工智能深度融合的路径探究[A]. 中共沈阳市委、沈阳市人民政府.第十七届沈阳科学学术年会论文集[C].中共沈阳市委、沈阳市人民政府:沈阳市科学技术协会,2020:5.
bibitem{ref4}柏卓君,潘勇,李仲余.彩色多普勒超声在早期胚胎停育诊断中的应用[J].影像研究与医学应用,2020,4(18):129-131.
bibitem{ref5}杨芸.我院2018年人血白蛋白临床应用调查与分析[J].上海医药,2020,41(17):34-35+74.

end{thebibliography} 
```

上面列出了5个参考文献，{thebibliography}的选项99指的是参考文献的个数最大为99，可以设置为别的数。

在正文中引用参考文献的方法是：

```latex
cite{ref1}

cite{ref1, ref5}
```

这样编译后就可以得到：

```
[1]

[1, 5]
```

## 使用BibTeX

这种方法需要建立参考文献数据库，引用的时候调用所需要的参考文献

BibTeX 是一种格式和一个程序，用于协调LaTeX的参考文献处理. BibTeX 使用数据库的的方式来管理参考文献. BibTeX 文件的后缀名为 .bib . 先来看一个例子

```bib
@article{name1,
author = {作者, 多个作者用 and 连接},
title = {标题},
journal = {期刊名},
volume = {卷20},
number = {页码},
year = {年份},
abstract = {摘要, 这个主要是引用的时候自己参考的, 这一行不是必须的}
}
@book{name2,
author ="作者",
year="年份2008",
title="书名",
publisher ="出版社名称"
} 
```

说明:

第一行@article 告诉 BibTeX 这是一个文章类型的参考文献，还有其它格式, 例如 article, book, booklet, conference, inbook, incollection, inproceedings，manual, misc, mastersthesis, phdthesis, proceedings, techreport, unpublished 等等.
接下来的"name1"，就是你在正文中应用这个条目的名称.
其它就是参考文献里面的具体内容啦.

在LaTeX中使用BibTeX

在论文最末，end{document}之前，插入如下两行命令：

```latex
bibliographystyle{plain}
bibliography{ref} 
```

命令1的作用是插入参考文献的样式，不同的杂志期刊的样式不一样，视个人情况。
命令2的作用是插入ref.bib文件
常见的预设样式的可选项有8种，分别是：

plain，按字母的顺序排列，比较次序为作者、年度和标题；  
unsrt，样式同plain，只是按照引用的先后排序；  
alpha，用作者名首字母+年份后两位作标号，以字母顺序排序；  
abbrv，类似plain，将月份全拼改为缩写，更显紧凑；  
ieeetr，国际电气电子工程师协会期刊样式；  
acm，美国计算机学会期刊样式；  
siam，美国工业和应用数学学会期刊样式；  
apalike，美国心理学学会期刊样式；  

引用文献：

```latex
cite {引用文章名称，例如name1} 
```

### 快速生成 bib 文件

Google Scholars 支持一键生成 bib 格式：

![](2022-05-04-18-20-48.png)

## 其他工具

如需管理大量文献，可以使用专业的工具例如 Endnote 和 Zotero。

当然这不是必要的。

参考：[超详细！文献管理软件对比——Endnote、Noteexpress、Zotero、Citavi](https://zhuanlan.zhihu.com/p/348608795)

## 如何编译

如果您使用 Overleaf，则程序会自动处理参考文献的编译问题。

如果您使用 VSCode，请在 `Build LaTeX project` 中，用 `xelatex -> bibtex -> xelatex*2` 或者 `pdflatex -> bibtex -> pdflatex*2` 编译

## 附录

### Reference

1. [在LaTeX中如何引用参考文献](https://segmentfault.com/a/1190000037460406)
2. [超详细！文献管理软件对比——Endnote、Noteexpress、Zotero、Citavi](https://zhuanlan.zhihu.com/p/348608795)

### 版权声明

本文原载于https://latex.all2doc.com/
