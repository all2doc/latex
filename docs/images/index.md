---
title: "LaTeX 图片处理"
slug: "Images"
date: 2022-05-04T12:43:49+08:00
draft: false
---


## 图片处理

对图片的处理需要引入

[LaTeX入门(八)——图片](https://zhuanlan.zhihu.com/p/56435541)

插入图片
要在文档中插入图片，需要graphicx宏包。此外，为了让图片出现在正确的位置，需要float宏包。因此，在导言区中加上

\usepackage{graphicx}
\usepackage{float}
接着，如果我们要插入的图片的名字是pic.png, 那么在我们正文中要插入图片的位置加上如下代码：

\begin{figure}[H]
\centering
\includegraphics{pic.png}
\caption{Title of picture}
\end{figure}
这里注意一点，如果要将图片插入到文档中，建议将图片拷贝到.tex文件的同一级文件夹中，否则需要在\includegraphics{}的参数中填写绝对路径。

而\caption{}指令的作用，是在其对应的位置（位于\includegraphics{}前就是图片上方，后就是图片下方）产生“图x：Title of picture”的语句。其中x是图片的编号。关于图片的编号，后面的文章会统一说。

可以给\includegraphics{}加上option列表以调整图片的大小。比如说，我想插入的图片是原图片等比例缩小为原来的0.8，则可以写成

\includegraphics[scale=0.8]{pic.png} 
如果我是想专门调整长宽，则可以把scale=0.8换成height=xxx, width=xxx这样来实现。

此外，关于\caption{}，我们可以对其字体、文字进行相关设置。这里需要我们使用caption宏包。我们首先在导言区中写上

\usepackage{caption}
然后在导言区中使用\captionsetup{}进行相关设置。

由于我们这里需要设置的是figure浮动体的caption，因此，我们需要特别注明captionsetup[figure]{}. 如果我们需要改变标题中的“图x”，让它变成“Picture x”或者其他文字，在大括号中可以写name=Picture来实现。如果我们要改变这个caption整体的字号、字体，比如说全都变成Large字号，意大利斜体，则可以在大括号中写font={Large, it}. 值得注意的是这里的字号只能设置成LaTeX内置的那几个字号（可以参见之前的文章），而不能自己设置字号。其他参数均可以看caption宏包的说明。

并排图片
将两个图片并排在一起，需要一定的技巧。假设我们有两张图片pic1.png和pic2.png, 我们要将它们并排，则可以在正文中使用如下语句：

\begin{figure}[H]
\centering
\begin{minipage}{0.48\textwidth}
\centering
\includegraphics{pic1.png}
\caption{Title of pic1}
\end{minipage}
\begin{minipage}{0.48\textwidth}
\centering
\includegraphics{pin2.png}
\caption{Title of pic2}
\end{minipage}
\end{figure}
如果我们要并排三张图，则将0.48\textwidth变成0.32\textwidth，以此类推。

过宽的图片
有时候我们会遇到过宽的图片，由于我们之前说的页面设置中存在左边距，所以会出现以下这种尴尬情况：


这时，我们需要使用一个叫adjustbox的宏包。在导言区加上一句

\usepackage[export]{adjustbox} 
然后在正文中使用

\begin{figure}[H]
\centering
\includegraphics[center]{pic.png}
\end{figure}
即可。也就是说，在\includegraphics[]{}的中括号中，加上一句center就能实现图片居中了。如图：


## 附录

### Reference

### 版权声明

本文原载于https://latex.all2doc.com/
