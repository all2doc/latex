---
title: "本地安装 LaTeX （VSCode）"
slug: "Vscode"
date: 2022-05-04T11:54:43+08:00
draft: false
---

此部分将介绍如何在本地安装 LaTeX 环境。

## 安装 LaTeX 编译器

TeX Live 是 TUG (TeX User Group) 维护和发布的 TeX 系统，可说是「官方」的 TeX 系统。我们推荐任何阶段的 TeX 用户，都尽可能使用 TeX Live，以保持在跨操作系统平台、跨用户的一致性。TeX Live 的官方站点是

[TeX Live - TeX Users Group](https://tug.org/texlive/)

Windows下，您可以直接使用

[Windows - TeX Live - TeX Users Group](https://tug.org/texlive/windows.html)

官方网站的安装工具，但TeXLive安装包大小共有4G，下载可能较慢。如果出现此情况，可以转到

[Index of /CTAN/systems/texlive/Images/ | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/texlive/Images/)

下载。

![](2022-05-03-11-07-00.png)

下载完成后，点击下载好的文件即可打开，再点击install-tl-windows.bat文件进行安装即可。

安装选项中，可以将“安装TeXworks前端”取消，其他保持默认即可。安装过程大约20分钟。

检查安装是否正常： 按win + R 打开运行，输入cmd，打开命令行窗口；然后输入命令xelatex -v ，如下图。若显示找不到命令，您可能需要重启计算机。

![](2022-05-04-15-36-54.png)

## VSCode配置Latex

如果您是首次接触VSCode，可以参考：[开始使用 - VSCode入门指南](https://vscode.all2doc.com/start/)，此处不做赘述。

### 安装插件

如图，安装 Latex Workshop 和 LaTeX language support 两个插件。

![](2022-05-03-12-58-21.png)

### 修改设置

转到`setting.json`（参考：[配置文件 - VSCode入门指南](https://vscode.all2doc.com/config/))，在最外层花括号内添加以下代码：

```json
    "latex-workshop.latex.autoBuild.run": "never",
    "latex-workshop.showContextMenu": true,
    "latex-workshop.intellisense.package.enabled": true,
    "latex-workshop.message.error.show": false,
    "latex-workshop.message.warning.show": false,
    "latex-workshop.latex.tools": [
        {
            "name": "xelatex",
            "command": "xelatex",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "%DOCFILE%"
            ]
        },
        {
            "name": "pdflatex",
            "command": "pdflatex",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "%DOCFILE%"
            ]
        },
        {
            "name": "latexmk",
            "command": "latexmk",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-pdf",
                "-outdir=%OUTDIR%",
                "%DOCFILE%"
            ]
        },
        {
            "name": "bibtex",
            "command": "bibtex",
            "args": [
                "%DOCFILE%"
            ]
        }
    ],
    "latex-workshop.latex.recipes": [
        {
            "name": "XeLaTeX",
            "tools": [
                "xelatex"
            ]
        },
        {
            "name": "PDFLaTeX",
            "tools": [
                "pdflatex"
            ]
        },
        {
            "name": "BibTeX",
            "tools": [
                "bibtex"
            ]
        },
        {
            "name": "LaTeXmk",
            "tools": [
                "latexmk"
            ]
        },
        {
            "name": "xelatex -> bibtex -> xelatex*2",
            "tools": [
                "xelatex",
                "bibtex",
                "xelatex",
                "xelatex"
            ]
        },
        {
            "name": "pdflatex -> bibtex -> pdflatex*2",
            "tools": [
                "pdflatex",
                "bibtex",
                "pdflatex",
                "pdflatex"
            ]
        },
    ],
    "latex-workshop.latex.clean.fileTypes": [
        "*.aux",
        "*.bbl",
        "*.blg",
        "*.idx",
        "*.ind",
        "*.lof",
        "*.lot",
        "*.out",
        "*.toc",
        "*.acn",
        "*.acr",
        "*.alg",
        "*.glg",
        "*.glo",
        "*.gls",
        "*.ist",
        "*.fls",
        "*.log",
        "*.fdb_latexmk"
    ],
    "latex-workshop.latex.autoClean.run": "onFailed",
    "latex-workshop.latex.recipe.default": "lastUsed",
    "latex-workshop.view.pdf.internal.synctex.keybinding": "double-click"
```

关于此代码的详细解释以及自定义配置请参考

### Hello World


在**纯英文目录下**，新建`HelloWorld.tex`文件，写入以下内容：

```latex
\documentclass{article}
\begin{document}
HelloWorld
\end{document}
```


打开此文件后，侧边导航栏显示TEX工作区：

![](2022-05-03-16-54-52.png)

点击`Recipe:XeLaTeX`编译，验证安装和配置情况。如无问题将会显示一个啥都没有的HelloWorld界面。

## Debug功能

点击`View Log message`-`View LaTeX compiler`（第一个是插件的log，第二个才是编译器的log），即可查看错误信息。

一旦获得了错误信息，您就可以使用Google或者必应进行错误排查了。

## 常见问题解决

### 是否正确安装

一般而言，完成 texlive 安装后，会自动修改 PATH 文件以便随时调用。

不过也存在 cmd 可以调用 XeLaTeX，VSCode 无法调用的情况。此时请重启电脑再次尝试。

### 编译器选择

当我们的文件不含引用时，请使用`XeLaTeX`或者`PDFLaTeX`，如果包含引用，请使用最后两个编译链。

更多内容可以参考：[编译器 - 基础知识](https://latex.all2doc.com/recipes/)

## 自动补全与错误显示

VSCode 中编写 LaTeX 一大好处是可以使用其自动补全与错误提示。安装 LaTeX Workshop 之后，这些功能就自动配置好了。

使用`\`开头，以触发关键词补全，例如，要输入

```latex
\begin{document}
\end{document}
```

只需输入`\doc...`。当然，也可以`\be...`然后选择合适的自动补全。

## 添加注释

和全局配置一致。`Ctrl+/`

## 配置文件详解

此部分将对配置文件进行详细介绍以及对其进行自定义。


```json
{
    // 不进行自动编译，可以设置为 onSave -当代码被保存时自动编译文件
    "latex-workshop.latex.autoBuild.run": "never",
    // 开启上下文菜单。设置为true时，右键菜单中增添两个选项。第一个选项为进行tex文件的编译，而第二个选项为进行正向同步，即从代码定位到编译出来的 pdf 文件相应位置
    "latex-workshop.showContextMenu": true,
    // 自动补全，必备
    "latex-workshop.intellisense.package.enabled": true,
    // 是否显示错误和警告弹窗。不必要，可以Debug获取
    "latex-workshop.message.error.show": false,
    "latex-workshop.message.warning.show": false,
    // 编译工具集，定义如何对你的源文件进行编译生成文档
    "latex-workshop.latex.tools": [
        // 定义多个编译工具
        {
            // 编译工具的名称
            "name": "xelatex",
            // 编译工具的运行命令
            "command": "xelatex",
            // 编译工具的参数合辑
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                // 可以更改的代码为，将编译方式: pdflatex 、 xelatex 和 latexmk 中的%DOCFILE更改为%DOC。%DOCFILE表明编译器访问没有扩展名的根文件名，而%DOC表明编译器访问的是没有扩展名的根文件完整路径。这就意味着，使用%DOCFILE可以将文件所在路径设置为中文，但笔者不建议这么做，因为毕竟涉及到代码，当其余编译器引用时该 tex 文件仍需要根文件完整路径，且需要为英文路径。笔者此处设置为%DOCFILE仅是因为之前使用 TeXstudio，导致路径已经是中文了。
                "%DOCFILE%"
            ]
        },
        {
            "name": "pdflatex",
            "command": "pdflatex",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "%DOCFILE%"
            ]
        },
        {
            "name": "latexmk",
            "command": "latexmk",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-pdf",
                "-outdir=%OUTDIR%",
                "%DOCFILE%"
            ]
        },
        {
            "name": "bibtex",
            "command": "bibtex",
            "args": [
                "%DOCFILE%"
            ]
        }
    ],
    // 此串代码是对编译链进行定义，其中name是标签，也就是出现在工具栏中的链名称；tool是name标签所对应的编译顺序，其内部编译命令来自上文latex-workshop.latex.recipes中内容。
    "latex-workshop.latex.recipes": [
        {
            "name": "XeLaTeX",
            "tools": [
                "xelatex"
            ]
        },
        {
            "name": "PDFLaTeX",
            "tools": [
                "pdflatex"
            ]
        },
        {
            "name": "BibTeX",
            "tools": [
                "bibtex"
            ]
        },
        {
            "name": "LaTeXmk",
            "tools": [
                "latexmk"
            ]
        },
        {
            "name": "xelatex -> bibtex -> xelatex*2",
            "tools": [
                "xelatex",
                "bibtex",
                "xelatex",
                "xelatex"
            ]
        },
        {
            "name": "pdflatex -> bibtex -> pdflatex*2",
            "tools": [
                "pdflatex",
                "bibtex",
                "pdflatex",
                "pdflatex"
            ]
        },
    ],
    // 这串命令则是设置编译完成后要清除掉的辅助文件类型，若无特殊需求，无需进行更改。
    "latex-workshop.latex.clean.fileTypes": [
        "*.aux",
        "*.bbl",
        "*.blg",
        "*.idx",
        "*.ind",
        "*.lof",
        "*.lot",
        "*.out",
        "*.toc",
        "*.acn",
        "*.acr",
        "*.alg",
        "*.glg",
        "*.glo",
        "*.gls",
        "*.ist",
        "*.fls",
        "*.log",
        "*.fdb_latexmk"
    ],
    // 这条命令是设置什么时候对上文设置的辅助文件进行清除。保持即可。1. onBuilt : 无论是否编译成功，都选择清除辅助文件；2. onFailed : 当编译失败时，清除辅助文件；3. never : 无论何时，都不清除辅助文件。
    "latex-workshop.latex.autoClean.run": "onFailed",
    // 该命令的作用为设置 vscode 编译 tex 文档时的默认编译链。有两个变量： 1. first :  使用latex-workshop.latex.recipes中的第一条编译链，故而您可以根据自己的需要更改编译链顺序； 2. lastUsed : 使用最近一次编译所用的编译链。
    "latex-workshop.latex.recipe.default": "lastUsed",
    // 用于反向同步（即从编译出的 pdf 文件指定位置跳转到 tex 文件中相应代码所在位置）的内部查看器的快捷键绑定。变量有两种： 1. ctrl-click ： 为默认选项，使用Ctrl/cmd+鼠标左键单击 2. double-click :  使用鼠标左键双击
    "latex-workshop.view.pdf.internal.synctex.keybinding": "ctrl-click"
}
```

## 附录

### Reference


1. [客_texlive安装需要多久](https://blog.csdn.net/qq_34769162/article/details/119752831)
2. [Visual Studio Code (vscode)配置LaTeX](https://zhuanlan.zhihu.com/p/166523064)
3. [LaTeX公式手册(全网最全) - 樱花赞 - 博客园](https://www.cnblogs.com/1024th/p/11623258.html)
4. [Latex中的各种文件及编译流程（附windows环境的完整编译脚本）_huitailangyz的博客-CSDN博客_latex编译](https://blog.csdn.net/huitailangyz/article/details/99685683)
5. [构建一个舒适的 LaTeX 工作流](https://blog.triplez.cn/posts/build-a-great-latex-workflow/)



### 版权声明

本文原载于https://latex.all2doc.com/
